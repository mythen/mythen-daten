# Mythology Data

## Installation

* `docker-compose up -d`

## Format Daten

1. Spalte Zeile (im Text)
2. Spalte HSPO-Feld (Vereinfachte Form des Satzes)
3. Spalte #S: Subject dann 1-n folgende Zellen Subjekt-Determination 
kann 1-m mal auftreten

X. Spalte #P: Prädikat dann 1-n folgende Prädikat-Determination kann 1x 
auftreten

X. Spalte #O: Object dann 0-n folgende Zellen Objekt-Determination kann 
1-m mal auftreten

## Checks

* [Number of HylemSequences](http://localhost:8983/solr/myth/select?facet.field=source&facet=on&q=*:*&group=true&group.field=source&group.limit=200&rows=200)
