#!/usr/bin/env python3

import re as regex
import os
import csv

# directory with csv files
path = "./csv-data/"


def csv_to_solr(file_reader, name):
    source: str = regex.sub("[\s+,.]", "_", name.replace(".csv", ""))
    counter: int = 1
    text_number: int = 1
    miss_subject: bool = True
    miss_predicate: bool = True
    csv_reader = csv.reader(file_reader, delimiter=';', quotechar='µ')

    for row in csv_reader:
        row_as_string: str = str(row)
        if row_as_string.find("Zeile") >= 0 or regex.search("^;+$", row_as_string) or not regex.search(".+#S:.+#P:.+", row_as_string):
            continue
        print("<doc>")
        print("<field name=\"id\">", counter, "-", source, "</field>", sep='')
        print("<field name=\"text_nr\">", text_number, "</field>", sep='')
        print("<field name=\"source\">" + source + "</field>")
        print("<field name=\"hylem_type\">hylem</field>")
        print("<field name=\"line_info\">" + row[0] + "</field>")
        if regex.search("^\"", row[1]):
            print("<field name=\"hspo\">" + regex.sub("\"", "", row[1]) + "</field>")
            print("<field name=\"speach\">true</field>")
        else:
            print("<field name=\"hspo\">" + row[1] + "</field>")
            print("<field name=\"speach\">false</field>")

        curr: str = ""
        p: str = ""
        for column in row:
            column.rstrip()

            if column.find("#S:") >= 0:
                p = "s"
                curr = regex.sub("#S:\s*", "", column)
                print("<field name=\"subject\">" + curr + "</field>")
                miss_subject = False
            elif column.find("#P:") >= 0:
                p = "p"
                curr = regex.sub("#P:\s*", "", column)
                print("<field name=\"predicate\">" + curr + "</field>")
                miss_predicate = False
            elif column.find("#O:") >= 0:
                p = "o"
                curr = regex.sub("#O:\s*", "", column)
                print("<field name=\"object\">" + curr + "</field>")
            elif column != "":
                if p == "s":
                    print("<field name=\"subject_determ\">" + column + "</field>")
                    print("<field name=\"subject_determ_ref\">" + curr + "</field>")
                elif p == "p":
                    print("<field name=\"predicate_determ\">" + column + "</field>")
                elif p == "o":
                    print("<field name=\"object_determ\">" + column + "</field>")
                    print("<field name=\"object_determ_ref\">" + curr + "</field>")
        print("</doc>")
        if miss_subject or miss_predicate:
            print("eRRR")
        miss_subject = True
        miss_predicate = True
        counter = counter + 1
        text_number = text_number + 1


################################
print("<add>")

for file_name in os.listdir(path):

    if os.path.isfile(os.path.join(path, file_name)):
        with open(path + file_name, newline='') as file:
            csv_to_solr(file, file_name)
print("</add>")
