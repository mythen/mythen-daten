FROM python:3.8 as indexer

WORKDIR /usr/src/app
COPY . /usr/src/app

RUN python3 csv2solr.py > docs.xml

FROM solr:8.7-slim
ENV SOLR_DATA_DIR=/index \
    SOLR_CORE=myth \
    SOLR_ROOT=/opt/solr \
    SOLR_HOME=/opt/solr/server/solr \
    SOLR_USER=solr \
    SOLR_GROUP=solr

USER root

RUN mkdir /index && \
    chown -R $SOLR_USER:$SOLR_GROUP $SOLR_DATA_DIR && \
    mkdir -p $SOLR_HOME/$SOLR_CORE/ && \
    chown -R $SOLR_USER:$SOLR_GROUP $SOLR_HOME/$SOLR_CORE/

USER $SOLR_USER

COPY --from=indexer /usr/src/app/docs.xml /usr/src/app/
COPY --chown=$SOLR_USER:$SOLR_GROUP ./solr-config/* $SOLR_HOME/$SOLR_CORE/

RUN touch $SOLR_HOME/$SOLR_CORE/core.properties && \
    solr start && \
    wait-for-solr.sh && \
    sleep 20 && \
    bin/post -c $SOLR_CORE  /usr/src/app/docs.xml
